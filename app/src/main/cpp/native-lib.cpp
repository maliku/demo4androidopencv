#include <jni.h>
#include <opencv2/opencv.hpp>

extern "C"
JNIEXPORT jint JNICALL
Java_com_tencent_demo4opencv_MainActivity_channelSplit(
        JNIEnv *env,
        jobject /* this */,
    jlong matIn) {
    cv::Mat* in = reinterpret_cast<cv::Mat*>(matIn);
    return in->channels();
}
