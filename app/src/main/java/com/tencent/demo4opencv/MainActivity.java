package com.tencent.demo4opencv;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.widget.ImageView;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.features2d.BFMatcher;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.Features2d;
import org.opencv.features2d.ORB;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;

import static org.opencv.core.CvType.CV_8UC4;

public class MainActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "MainActivity";

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
        OpenCVLoader.initDebug();
    }

    private ImageView mImageView1;
    private ImageView mImageView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mImageView1 = (ImageView) findViewById(R.id.imgView1);
        mImageView2 = (ImageView) findViewById(R.id.imgView2);
        findViewById(R.id.okBtn).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
/*        Mat img = null;
        try {
            img = Utils.loadResource(this, R.drawable.food, CV_8UC4);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (null != img) {
            Bitmap bmp = Bitmap.createBitmap(img.width(), img.height(), Bitmap.Config.ARGB_8888);
            long start = SystemClock.uptimeMillis();

            Log.d(TAG, "Mat channels = " + channelSplit(img.getNativeObjAddr()));
            Imgproc.cvtColor(img, img, Imgproc.COLOR_BGRA2RGBA);
            Utils.matToBitmap(img, bmp);
            Log.d(TAG, "mat to bitmap cost=" + (SystemClock.uptimeMillis() - start));
            mImageView1.setImageBitmap(bmp);
        }*/
        mImageView1.setImageResource(R.drawable.store);
        mImageView2.setImageResource(R.drawable.store_label);
    }

    boolean matchStore() throws IOException {
        Mat label = Utils.loadResource(this, R.drawable.store_label, CV_8UC4);
        Mat img = Utils.loadResource(this, R.drawable.store, CV_8UC4);

        ORB orb = ORB.create(1000, 1.4f, 1, 31, 0, 2, ORB.FAST_SCORE, 31, 25);
        MatOfKeyPoint kpLabel = new MatOfKeyPoint();
        Mat descLabel = new Mat();
        Mat descImg = new Mat();
        MatOfKeyPoint kpImg = new MatOfKeyPoint();

        DescriptorMatcher bfMather = BFMatcher.create(BFMatcher.BRUTEFORCE_HAMMING);
        MatOfDMatch matches = new MatOfDMatch();

        long begin = SystemClock.uptimeMillis();
        long start = begin;
        long tt;

        Imgproc.resize(label, label, new org.opencv.core.Size(360, 120));
        img = new Mat(img, new Rect(10, 40, 120, 40));
        Imgproc.resize(img, img, new org.opencv.core.Size(360, 120));

        tt = start; start = SystemClock.uptimeMillis();
        Log.d(TAG, "Resize cost=" + (start - tt));

        orb.detectAndCompute(label, new Mat(), kpLabel, descLabel);
        orb.detectAndCompute(img, new Mat(), kpImg, descImg);

        tt = start; start = SystemClock.uptimeMillis();
        Log.d(TAG, "Detect&Compute cost=" + (start - tt));

        bfMather.match(descImg, descLabel, matches);

        tt = start; start = SystemClock.uptimeMillis();
        Log.d(TAG, "Force match cost=" + (start - tt));
        Log.d(TAG, "Total cost=" + (start - begin));

        Log.i(TAG, "Matches = " + matches.toList().size());

        Features2d.drawKeypoints(img, kpImg, img);
        Features2d.drawKeypoints(label, kpLabel, label);

        Imgproc.cvtColor(img, img, Imgproc.COLOR_BGRA2RGBA);
        Imgproc.cvtColor(label, label, Imgproc.COLOR_BGRA2RGBA);

        Bitmap imgBmp = Bitmap.createBitmap(img.width(), img.height(), Bitmap.Config.ARGB_8888);
        Bitmap labelBmp = Bitmap.createBitmap(label.width(), label.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(img, imgBmp);
        Utils.matToBitmap(label, labelBmp);

        mImageView1.setImageBitmap(imgBmp);
        mImageView2.setImageBitmap(labelBmp);

        Mat outImg = new Mat();
        Features2d.drawMatches(img, kpImg, label ,kpLabel, matches, outImg);
        imgBmp = Bitmap.createBitmap(outImg.width(), outImg.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(outImg, imgBmp);
        mImageView1.setImageBitmap(imgBmp);

        return false;
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native int channelSplit(long mat);

    @Override
    public void onClick(View view) {
        try {
            matchStore();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
